# Pointer division and modulo

Function divides parameters e by f and stores the result in the int pointed by divi. It also stores the remainder of the division of e by f in the int pointed by modu.